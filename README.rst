README
===================
This is the README file of the ``common`` folder, a folder that
contains useful support information to create slides with LaTeX and
beamer.

.. warning::
   This template is not officially supported by the University of Milan.

What is this repository for?
********************************
* This repository contains useful LaTeX style files and images to support the creation of slides.

How do I get set up?
**************************
* Copy this folder in some location easily accessible from the
  location of your TEX file containing the slides (in general, for
  example for a course, I put one copy of this folder in the root
  directory containing the folders of each lesson)
* For example usage, see also the repository `template_beamer`.

Maintainer
******************************
* Alessando Comunian

